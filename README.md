# eapolcfg

eapolcfg is a command line tool to configure EAP on macOS.

## Usage 
> usage: eapolcfg <command> <args>

> where <command> is one of:

> 	listProfiles	

> 	showProfile

> 	exportProfile

> 	removeProfile

> 	importProfile

> 	createProfile

> 	setProfileInformation

> 	getProfileInformation

> 	clearProfileInformation

> 	setPasswordItem

> 	getPasswordItem

> 	removePasswordItem

> 	setIdentity

> 	clearIdentity

> 	getIdentity

> 	getLoginWindowProfiles

> 	setLoginWindowProfiles

> 	clearLoginWindowProfiles

> 	getSystemProfile

> 	setSystemProfile

> 	getSystemEthernetProfile

> 	setSystemEthernetProfile

> 	clearSystemEthernetProfile

> 	clearSystemProfile

> 	getLoginWindowInterfaces

> 	getSystemInterfaces

> 	startAuthentication


## Compiling

The source for eapolcfg is part of the eap8021x project on opensource.apple.com:

https://opensource.apple.com/source/eap8021x/

The project has many private headers and internal dependencies so eapolcfg.c in the eapolcfg.tproj folder has been modified to reference included headers. The Makefile has been modified to build for Intel and Apple Silicon and reference the included headers. The headers are from multiple other projects, including SystemConfiguration and other parts of the eap8021x project. The headers/EAP8021X folder is a paired down version of the EAP8021X.fproj folder with only the needed headers.

To build:

`make all`

To clean:

`make clean`

The binary should be native for both intel and Apple Silicon.

See the Makefile to build separate architectures.

## whoami

Follow me on twitter at @tperfitt, join the MacAdmins slack at https://macadmins.org. If you want help incorporating this or other projects, please reach out at https://twocanoes.com/contact/
